import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    public static int add(int value1, int value2) {
        return value1 + value2;
    }

    public int subtract(int value1, int value2) {
        return value1 - value2;
    }

    public int multiply(int value1, int value2) {
        return value1 * value2;
    }

    public int divide(int value1, int value2) throws invalidEntryException {
        if (value2 == 0) {throw new invalidEntryException();}
        return value1 / value2;
    }
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
    }

    @Test
    public void exceptionTesting() {

        assertThrows(invalidEntryException.class, () -> divide(3,0));
    }

    @Test
    @DisplayName("Simple addition should work")
    void testAdd() {
        assertEquals(20, add(10,10), "Regular addition should work");
    }

    @Test
    @DisplayName("Simple subtraction should work")
    void testSubtract() {
        assertEquals(-4, subtract(10,14), "Regular subtraction should work");
    }

    @Test
    @DisplayName("Simple multiplication should work")
    void testMultiply() {
        assertEquals(100, multiply(10,10), "Regular multiplication should work");
    }

    @Test
    @DisplayName("Simple division should work")
    void testDivide() throws invalidEntryException {
        assertEquals(1, divide(10,10), "Regular division should work");
    }

    class MyClass {
        public int add(int i,int j) {
            return i + j;
        }
    }












}